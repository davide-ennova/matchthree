//
//  MAGGameController.m
//  MatchThree
//
//  Created by Vincenzo Alampi on 25/11/14.
//  Copyright (c) 2014 None. All rights reserved.
//

#import "MAGGameController.h"
#import "MAGLevelLoader.h"
#import "MAGGrid.h"
#import "MAGSelection.h"
#import "MAGObject.h"
#import "MAGObjectsAnimator.h"
#import "MAGSelectionDrawNode.h"

#define K_MIN_SELECTION_COUNT 3
#define K_REDUCTION_FACTOR 8

@interface MAGGameController()

@property (strong, nonatomic) CCScene *scene;
@property (strong, nonatomic) MAGLevel *levelInfo;
@property (strong, nonatomic) MAGGrid *grid;

@property (strong, nonatomic) MAGGridTouchHandler *gridTouchHandler;
@property (strong, nonatomic) MAGSelection *selection;
@property (strong, nonatomic) MAGSelectionDrawNode *selectionDrawNode;

@property (strong, nonatomic) MAGGridRefiller *gridRefiller;

@end

@implementation MAGGameController

+ (MAGGameController*) gameWithLevel:(NSInteger)level scene:(CCScene*)scene
{
    return [[self alloc] initWithLevel:level scene:scene];
}

- (id) initWithLevel:(NSInteger)level scene:(CCScene*)scene
{
    if (self = [super init])
    {
        self.scene = scene;
        
        self.levelInfo = [MAGLevelLoader loadLevel:1];
        self.grid = [MAGGrid gridData:self.levelInfo scene:self.scene];
        
        self.gridTouchHandler = [MAGGridTouchHandler createTouchHandler:self.grid.gridSize];
        self.gridTouchHandler.delegate = self;
        
        [self.scene addChild:self.gridTouchHandler];
        
        self.selection = [MAGSelection createSelection];
        
        self.selectionDrawNode = [MAGSelectionDrawNode drawSelection:self.selection];
        [self.scene addChild:self.selectionDrawNode z:0];
        
        self.gridRefiller = [MAGGridRefiller createRefiller];
        self.gridRefiller.delegate = self;
    }
    
    return self;
}


#pragma mark MAGGridSelectionProtocol

- (void) handleSelectionOfPosition:(CGPoint)touchLoc
{
    CGPoint touchGridLoc = [self convertToGrid:touchLoc];
    
    if (![self isValidGridPosition:touchGridLoc])
    {
        return;
    }
    
    MAGObject *object = [self.grid objectAtCol:touchGridLoc.x row:touchGridLoc.y];
    
    CGPoint differenceFromCenterObject = CGPointMake(object.sprite.boundingBox.size.width * 0.5f, object.sprite.boundingBox.size.height * 0.5f);
    
    touchLoc = ccpAdd (touchLoc, self.grid.startingPosition);
    touchLoc = ccpSub(touchLoc, differenceFromCenterObject);
    
    CGRect objectTouchableRect = CGRectInset(object.sprite.boundingBox, K_REDUCTION_FACTOR, K_REDUCTION_FACTOR);
    
    if (CGRectContainsPoint(objectTouchableRect, touchLoc))
    {
        [self.selection handleSelectionOf:object];
    }
}

- (void) evaluateSelection
{
    NSMutableArray *selection = [self.selection accessCollection];
    NSMutableArray *columnsToFill = [[NSMutableArray alloc] initWithCapacity:7];
    
    if (selection.count >= K_MIN_SELECTION_COUNT)
    {
        NSMutableArray *content = [self.grid dataContent];
        
        for (MAGObject *obj in selection)
        {
            NSInteger col = obj.column;
            NSInteger row = obj.row;
            
            [obj.sprite stopAllActions];
            
            [MAGObjectsAnimator collectAnimation:obj.sprite notify:self.gridRefiller];
            [[content objectAtIndex:col] replaceObjectAtIndex:row withObject:[NSNull null]];
            
            if (![columnsToFill containsObject:[NSNumber numberWithInteger:col]])
             {
                 [columnsToFill addObject:[NSNumber numberWithInteger:col]];
             }
        }
        [self.gridRefiller setupColumnsToRefill:columnsToFill data:content];
    }
    else
    {
        [self changeStateTo:Free];
    }
    
    [self.selection clearSelection];
}

- (CGPoint) convertToGrid:(CGPoint)position
{
    float objectSize = self.grid.gridSize.width / Columns;
    
    return CGPointMake(position.x / objectSize, position.y / objectSize);
}

- (BOOL) isValidGridPosition:(CGPoint)position
{
    return (position.x > 0 && position.y > 0 && position.x < Columns && position.y < Rows);
}

- (BOOL) isGridState:(GridState)state
{
    return self.grid.state == state;
}

- (void) changeStateTo:(GridState)state
{
    [self.grid setState:state];
}

#pragma mark MAGGridRefillProtocol

- (void) refillCol:(NSInteger)col row:(NSInteger)row
{
    MAGObjectType type = (MAGObjectType)arc4random_uniform(5) + 1;
    MAGObject *object = [MAGObject createObject: type];
    [self.grid addObject:object toCol:col row:row];
    
    [MAGObjectsAnimator refillingAnimation:object.sprite notify:self.gridRefiller];
}

@end