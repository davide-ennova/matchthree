//
//  MAGMapTouchHandler.m
//  MatchThree
//
//  Created by Vincenzo Alampi on 21/11/14.
//  Copyright (c) 2014 None. All rights reserved.
//

#import "MAGGridTouchHandler.h"

@implementation MAGGridTouchHandler

#pragma mark Creation

+ (MAGGridTouchHandler*) createTouchHandler:(CGSize)size
{
    return [[self alloc] initWithSize:size];
}

- (id) initWithSize:(CGSize)size
{
    if (self = [super init])
    {
        [self setUserInteractionEnabled:YES];
        [self setAnchorPoint:ccp(0.5f, 0.5f)];
        [self setPositionType: CCPositionTypeNormalized];
        [self setPosition:ccp(0.5f, 0.5f)];
        [self setContentSize:size];
    }
    return self;
}

#pragma mark Touch Handlers

- (void) touchBegan:(UITouch *)touch withEvent:(UIEvent *)event
{
    if ([self.delegate isGridState:Free])
    {
        CGPoint touchLocation = [touch locationInNode:self];
        [self.delegate changeStateTo:Selecting];
        [self.delegate handleSelectionOfPosition:touchLocation];
    }
}

- (void) touchMoved:(UITouch *)touch withEvent:(UIEvent *)event
{
    if ([self.delegate isGridState:Selecting])
    {
        CGPoint touchLocation = [touch locationInNode:self];
        [self.delegate handleSelectionOfPosition:touchLocation];
    }
}

- (void) touchEnded:(UITouch *)touch withEvent:(UIEvent *)event
{
    [self.delegate changeStateTo:Evaluating];
    [self.delegate evaluateSelection];
}

@end