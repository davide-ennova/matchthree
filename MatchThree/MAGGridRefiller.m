//
//  MAGGridRefiller.m
//  MatchThree
//
//  Created by Vincenzo Alampi on 26/11/14.
//  Copyright (c) 2014 None. All rights reserved.
//

#import "MAGGridRefiller.h"
#import "MAGObject.h"
#import "MAGObjectsAnimator.h"

@interface MAGGridRefiller()

@property (strong, nonatomic) NSMutableArray *dataContent;
@property (strong, nonatomic) NSMutableArray *columns;

@end

@implementation MAGGridRefiller

+ (MAGGridRefiller*) createRefiller
{
    return [[self alloc] init];
}

- (id) init
{
    if (self = [super init])
    {
        self.dataContent = [[NSMutableArray alloc] init];
        self.columns = [[NSMutableArray alloc] init];
    }
    return self;
}

- (void) setupColumnsToRefill:(NSMutableArray*)columns data:(NSMutableArray*)dataContent
{
    self.columns = columns;
    self.dataContent = dataContent;
}

- (void) startPullDownPhase
{
    [self.delegate changeStateTo:Refilling];
    
    BOOL thereIsSomethingToPullDown = NO;

    for (int i = 0; i < self.columns.count; i++)
    {
        NSInteger column = [[self.columns objectAtIndex:i] integerValue];
        NSMutableArray *rows = [self.dataContent objectAtIndex:column];
        
        thereIsSomethingToPullDown = [self pullDownColumn:column withRows:rows];
    }
    
    if (!thereIsSomethingToPullDown)
    {
        [self startRefillEmptyObjectsPhase];
    }
}

- (BOOL) pullDownColumn:(NSInteger)col withRows:(NSMutableArray*)rows
{
    BOOL somethingToPullDown = NO;
    
    for (NSInteger i = 0; i < rows.count; i++)
    {
        if ([rows objectAtIndex:i] == [NSNull null])
        {
            for (NSInteger j = i + 1; j < rows.count; j++)
            {
                if ([[rows objectAtIndex:j] isKindOfClass:[MAGObject class]])
                {
                    somethingToPullDown = YES;
                    
                    MAGObject *objectToMove = [rows objectAtIndex:j];
                    objectToMove.row = i;
                    
                    NSInteger differenceInRow = j - i;
                    
                    float height = objectToMove.sprite.boundingBox.size.height;
                    
                    CGPoint newPosition = ccp(0, height * differenceInRow);
                    
                    [MAGObjectsAnimator fallingAnimation:objectToMove.sprite toPosition:ccpSub(objectToMove.sprite.position, newPosition) notify:self];
                    
                    [rows replaceObjectAtIndex:i withObject:objectToMove];
                    [rows replaceObjectAtIndex:j withObject:[NSNull null]];
                    
                    break;
                }
            }
        }
    }
    
    return somethingToPullDown;
}

- (void) startRefillEmptyObjectsPhase
{
    for (int i = 0; i < self.columns.count; i++)
    {
        NSInteger column = [[self.columns objectAtIndex:i] integerValue];
        NSMutableArray *rows = [self.dataContent objectAtIndex:column];
        
        [self refillColumn:column withRows:rows];
    }
}

- (void) refillColumn:(NSInteger)col withRows:(NSMutableArray*)rows
{
    for (NSInteger i = rows.count - 1 ; i > 0; i--)
    {
        if ([rows objectAtIndex:i] == [NSNull null])
        {
            [self.delegate refillCol:col row:i];
        }
    }    
}

- (void) finishRefillPhase
{
    [self.delegate changeStateTo:Free];
}

@end