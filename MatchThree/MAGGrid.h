//
//  MAGGrid.h
//  MatchThree
//
//  Created by Vincenzo Alampi on 25/11/14.
//  Copyright (c) 2014 None. All rights reserved.
//

#import <Foundation/Foundation.h>

static const NSInteger Columns = 7;
static const NSInteger Rows = 7;

@class CCScene;
@class MAGLevel;
@class MAGObject;

typedef enum  {
    
    Free,
    Selecting,
    Evaluating,
    Refilling
    
} GridState;

@interface MAGGrid : NSObject

@property (assign, nonatomic) GridState state;
@property (assign, nonatomic) CGPoint startingPosition;
@property (assign, nonatomic) CGSize gridSize;

+ (MAGGrid*) gridData:(MAGLevel*)level scene:(CCScene*)scene;
- (NSMutableArray*) dataContent;

- (void) addObject:(MAGObject*)object toCol:(NSInteger)col row:(NSInteger)row;
- (MAGObject*) objectAtCol:(NSInteger)col row:(NSInteger)row;

@end