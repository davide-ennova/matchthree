//
//  MAGSelection.m
//  MatchThree
//
//  Created by Vincenzo Alampi on 22/11/14.
//  Copyright (c) 2014 None. All rights reserved.
//

#import "MAGSelection.h"
#import "MAGObjectsAnimator.h"
#import "MAGGameController.h"

@interface MAGSelection()

@property (assign, nonatomic) CGPoint lastSelectedObjectPosition;
@property (strong, nonatomic) NSMutableArray *collection;
@property (assign, nonatomic) MAGObjectType selectionType;

@end

@implementation MAGSelection

+ (MAGSelection*) createSelection
{
    return [[self alloc] init];
}

- (id) init
{
    if (self = [super init])
    {
        self.collection = [[NSMutableArray alloc] init];
    }
    
    return self;
}

#pragma mark Selection Methods

- (void) handleSelectionOf:(MAGObject*)obj
{
    if (self.selectionType == none)
    {
        [self setSelectionType:obj.type];
        [self setLastSelectedObjectPosition:CGPointMake(obj.row, obj.column)];
    }
    
    if (self.selectionType != obj.type)
    {
        return;
    }
    
    if (obj.isSelected && self.collection.count > 1)
    {
        MAGObject *lastObj = [self.collection objectAtIndex:self.collection.count - 2];
        
        if ([obj isEqual:lastObj])
        {
            MAGObject *veryLastObj = [self.collection lastObject];
            [self deselectObject:veryLastObj];
            [self setLastSelectedObjectPosition:CGPointMake(lastObj.row, lastObj.column)];
        }
    }
    
    if (obj.isSelected)
    {
        return;
    }
    
    float distance_x = abs(self.lastSelectedObjectPosition.x - obj.row);
    float distance_y = abs(self.lastSelectedObjectPosition.y - obj.column);

    if (distance_x > 1 || distance_y > 1)
    {
        return;
    }
    
    [self setLastSelectedObjectPosition:CGPointMake(obj.row, obj.column)];
    [self selectObject:obj];
}

- (void) selectObject:(MAGObject*)obj
{
    [obj setIsSelected:YES];
    [self.collection addObject:obj];
    
    [MAGObjectsAnimator selectAnimation:obj.sprite];
}

- (void) deselectObject:(MAGObject*)obj
{
    [obj setIsSelected:NO];
    [self.collection removeObject:obj];
    
    [MAGObjectsAnimator deselectAnimation:obj.sprite];
}

#pragma mark Utilities

- (NSMutableArray*) accessCollection
{
    return self.collection;
}

- (MAGObjectType) accessSelectionType
{
    return self.selectionType;
}

- (void) clearSelection
{
    for(MAGObject* obj in self.collection)
    {
        [obj setIsSelected:NO];
        [MAGObjectsAnimator deselectAnimation:obj.sprite];
    }
    
    self.selectionType = none;
    [self.collection removeAllObjects];
}

@end