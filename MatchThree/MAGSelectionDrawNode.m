//
//  MAGDrawNode.m
//  MatchThree
//
//  Created by Vincenzo Alampi on 23/11/14.
//  Copyright (c) 2014 None. All rights reserved.
//

#import "MAGSelectionDrawNode.h"
#import "MAGSelection.h"
#import "MAGObject.h"

@interface MAGSelectionDrawNode()

@property (weak, nonatomic) MAGSelection *selection;
@property (weak, nonatomic) NSMutableArray *collectionToDraw;
@property (weak, nonatomic) CCColor *color;

@end

@implementation MAGSelectionDrawNode

#pragma mark Creation

+ (MAGSelectionDrawNode*) drawSelection:(MAGSelection*)selection
{
    return [[self alloc] initWithSelection:selection];
}

- (id) initWithSelection:(MAGSelection*)selection
{
    if (self = [super init])
    {
        self.selection = selection;
        self.collectionToDraw = [selection accessCollection];
    }
    
    return self;
}

#pragma mark Drawing

- (void) draw:(CCRenderer *)renderer transform:(const GLKMatrix4 *)transform
{
    [self clear];
    
    if (self.collectionToDraw.count > 1)
    {
        MAGObjectType type = [self.selection accessSelectionType];
        
        CCColor *color = [self selectionTypeToColor:type];
        
        for (int i = 1; i < self.collectionToDraw.count; i++)
        {
            MAGObject *obj = [self.collectionToDraw objectAtIndex: i - 1];
            MAGObject *obj2 = [self.collectionToDraw objectAtIndex: i];
            
            [self drawSegmentFrom:obj.sprite.position to:obj2.sprite.position radius:10.0f color:color];
        }
    }
    
    [super draw:renderer transform:transform];
}

- (CCColor*)selectionTypeToColor:(MAGObjectType)type
{
    switch (type)
    {
        case amber: return [CCColor yellowColor]; break;
        case emerald: return [CCColor greenColor]; break;
        case prism: return [CCColor cyanColor]; break;
        case sapphire: return [CCColor blueColor]; break;
        case ruby: return [CCColor redColor]; break;
            
        default: break;
    }
    
    return [CCColor clearColor];
}

@end