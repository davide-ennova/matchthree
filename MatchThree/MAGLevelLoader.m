//
//  MAGLevelLoader.m
//  MatchThree
//
//  Created by Vincenzo Alampi on 23/11/14.
//  Copyright (c) 2014 None. All rights reserved.
//

#import "MAGLevelLoader.h"

@implementation MAGLevelLoader

+ (MAGLevel*) loadLevel:(NSInteger)lev
{
    return [MAGLevel createLevel:lev];
}

@end
