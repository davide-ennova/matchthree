//
//  MGObject.h
//  MatchThree
//
//  Created by Vincenzo Alampi on 21/11/14.
//  Copyright (c) 2014 None. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "cocos2d.h"

typedef enum
{
    none,
    amber,
    emerald,
    prism,
    sapphire,
    ruby
    
} MAGObjectType;

@interface MAGObject : NSObject

@property (strong, nonatomic) CCSprite *sprite;
@property (assign, nonatomic) MAGObjectType type;
@property (assign, nonatomic) BOOL isSelected;

@property (assign, nonatomic) NSInteger column;
@property (assign, nonatomic) NSInteger row;

+ (MAGObject*) createObject:(MAGObjectType)type;

@end
