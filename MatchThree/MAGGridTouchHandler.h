//
//  MAGMapTouchHandler.h
//  MatchThree
//
//  Created by Vincenzo Alampi on 21/11/14.
//  Copyright (c) 2014 None. All rights reserved.
//

#import "CCNode.h"
#import "cocos2d.h"
#import "MAGGrid.h"

@protocol MAGGridSelectionProtocol <NSObject>

- (BOOL) isGridState:(GridState)state;
- (void) handleSelectionOfPosition:(CGPoint)touchLoc;
- (void) evaluateSelection;
- (void) changeStateTo:(GridState)state;

@end

@interface MAGGridTouchHandler : CCNode

@property (assign, nonatomic) id <MAGGridSelectionProtocol> delegate;

+ (MAGGridTouchHandler*) createTouchHandler:(CGSize)size;

@end