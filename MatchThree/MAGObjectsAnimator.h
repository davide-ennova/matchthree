//
//  MAGObjectsAnimator.h
//  MatchThree
//
//  Created by Vincenzo Alampi on 22/11/14.
//  Copyright (c) 2014 None. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "cocos2d.h"

@class MAGGridRefiller;

@interface MAGObjectsAnimator : NSObject

+ (void) selectAnimation:(CCNode*)node;
+ (void) deselectAnimation:(CCNode*)node;

+ (void) collectAnimation:(CCNode*)node notify:(MAGGridRefiller*)refiller;
+ (void) fallingAnimation:(CCNode*)node toPosition:(CGPoint)position notify:(MAGGridRefiller*)refiller;
+ (void) refillingAnimation:(CCNode*)node notify:(MAGGridRefiller*)refiller;

@end