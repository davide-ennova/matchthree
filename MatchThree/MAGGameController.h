//
//  MAGGameController.h
//  MatchThree
//
//  Created by Vincenzo Alampi on 25/11/14.
//  Copyright (c) 2014 None. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "cocos2d.h"
#import "MAGGridTouchHandler.h"
#import "MAGGridRefiller.h"

@class MAGLevel;

@interface MAGGameController : NSObject <MAGGridSelectionProtocol, MAGGridRefillProtocol>

+ (MAGGameController*) gameWithLevel:(NSInteger)level scene:(CCScene*)scene;

@end