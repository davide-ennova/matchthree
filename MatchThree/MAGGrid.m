//
//  MAGGrid.m
//  MatchThree
//
//  Created by Vincenzo Alampi on 25/11/14.
//  Copyright (c) 2014 None. All rights reserved.
//

#import "MAGGrid.h"
#import "cocos2d.h"
#import "MAGLevel.h"
#import "MAGObject.h"

@interface MAGGrid()

@property (weak, nonatomic) MAGLevel *level;
@property (weak, nonatomic) CCScene *scene;
@property (strong, nonatomic) NSMutableArray *content;

@end

@implementation MAGGrid

+ (MAGGrid*) gridData:(MAGLevel*)level scene:(CCScene*)scene
{
    return [[self alloc] initData:level scene:(CCScene*)scene];
}

- (id) initData:(MAGLevel*)level scene:(CCScene*)scene
{
    if (self = [super init])
    {
        self.level = level;
        self.scene = scene;
        self.content = [[NSMutableArray alloc] initWithCapacity:Columns];
        
        CGSize screenSize = [CCDirector sharedDirector].viewSize;
        
        CCSprite *sprite = [CCSprite spriteWithImageNamed:@"amber.png"];
        
        float gridSide = sprite.boundingBox.size.width * Columns;
        float differenceWidth = screenSize.width - gridSide;
        float differenceHeight = screenSize.height - gridSide;
        
        float leftDistance = differenceWidth * 0.5f + 43.0f;
        float bottomDistance = differenceHeight * 0.5f + 43.0f;
        
        self.startingPosition = ccp(leftDistance, bottomDistance);
        self.gridSize = CGSizeMake(gridSide, gridSide);
        
        [self generateGrid];
    }
    return self;
}

- (void) generateGrid
{
    NSMutableArray *levelContent = [self.level levelContent];
    
    for (int i = 0; i < Columns; i++)
    {
        NSMutableArray *row = [[NSMutableArray alloc] initWithCapacity:Rows];
        
        [self.content addObject:row];
        
        for (int j = 0; j < Rows; j++)
        {
            MAGObjectType type = [[[levelContent objectAtIndex:i] objectAtIndex:j] intValue];
            MAGObject *object = [MAGObject createObject:type];
            
            [self addObject:object toCol:i row:j];
        }
    }
}

- (NSMutableArray*) dataContent
{
    return self.content;
}

- (void) addObject:(MAGObject*)object toCol:(NSInteger)col row:(NSInteger)row
{
    float x_position = self.startingPosition.x + object.sprite.boundingBox.size.width * col;
    float y_position = self.startingPosition.y + object.sprite.boundingBox.size.height * row;
    
    [object.sprite setPosition:ccp(x_position, y_position)];
    [object setColumn:col];
    [object setRow:row];
    
    if ([self.content count] == Columns)
    {
        if ([[self.content lastObject] count] == Rows)
        {
            [[self.content objectAtIndex:col] replaceObjectAtIndex:row withObject:object];
            [self.scene addChild:object.sprite z:1];
            return;
        }
    }

    [[self.content objectAtIndex:col] addObject:object];
    [self.scene addChild:object.sprite z:1];
}

- (MAGObject*) objectAtCol:(NSInteger)col row:(NSInteger)row
{
    return [[self.content objectAtIndex:col] objectAtIndex:row];
}

@end