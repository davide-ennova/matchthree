//
//  MAGLevel.m
//  MatchThree
//
//  Created by Vincenzo Alampi on 23/11/14.
//  Copyright (c) 2014 None. All rights reserved.
//

#import "MAGLevel.h"
#import "MAGObject.h"

@interface MAGLevel()

@property (assign, nonatomic) NSInteger level;

@end

@implementation MAGLevel

+ (MAGLevel*) createLevel:(NSInteger)lev
{
    return [[self alloc] initWithLevel:lev];
}

- (id) initWithLevel:(NSInteger)lev
{
    if (self = [super init])
    {
        self.level = lev;
    }
    return self;
}

- (NSMutableArray*) levelContent
{
    NSMutableArray *content = [[NSMutableArray alloc] initWithCapacity:7];
    
    for (int i = 0; i < 7; i++)
    {
        NSMutableArray *row = [[NSMutableArray alloc] initWithCapacity:7];
        
        [content addObject:row];
        
        for (int j = 0; j < 7; j++)
        {
            MAGObjectType type = (MAGObjectType)arc4random_uniform(5) + 1;
            [[content objectAtIndex:i] addObject:[NSNumber numberWithInt:type]];
        }
    }
    
    return content;
}

@end
