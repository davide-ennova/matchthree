//
//  MAGLevel.h
//  MatchThree
//
//  Created by Vincenzo Alampi on 23/11/14.
//  Copyright (c) 2014 None. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface MAGLevel : NSObject

+ (MAGLevel*) createLevel:(NSInteger)lev;
- (NSMutableArray*) levelContent;

@end
