//
//  MAGSelection.h
//  MatchThree
//
//  Created by Vincenzo Alampi on 22/11/14.
//  Copyright (c) 2014 None. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "MAGObject.h"

@class MAGObject;

@interface MAGSelection : NSObject

+ (MAGSelection*) createSelection;

- (void) handleSelectionOf:(MAGObject*)obj;

- (NSMutableArray*) accessCollection;
- (MAGObjectType) accessSelectionType;

- (void) clearSelection;

@end