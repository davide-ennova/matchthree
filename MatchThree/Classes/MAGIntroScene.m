//
//  IntroScene.m
//  MatchThree
//
//  Created by Vincenzo Alampi on 19/11/14.
//  Copyright None 2014. All rights reserved.
//
// -----------------------------------------------------------------------

#import "MAGIntroScene.h"
#import "MAGGameScene.h"

@implementation MAGIntroScene

+ (MAGIntroScene *)scene
{
	return [[self alloc] init];
}

- (id)init
{
    self = [super init];
    if (!self) return(nil);
    
    CCNodeColor *background = [CCNodeColor nodeWithColor:[CCColor colorWithRed:0.2f green:0.2f blue:0.2f alpha:1.0f]];
    [self addChild:background];
    
    CCLabelTTF *label = [CCLabelTTF labelWithString:@"Hi MAG Team !" fontName:@"Verdana-Bold" fontSize:36.0f];
    label.positionType = CCPositionTypeNormalized;
    label.color = [CCColor redColor];
    label.position = ccp(0.5f, 0.5f);
    [self addChild:label];
    
    CCButton *playButton = [CCButton buttonWithTitle:@"Play" fontName:@"Verdana-Bold" fontSize:36.0f];
    playButton.positionType = CCPositionTypeNormalized;
    playButton.position = ccp(0.5f, 0.35f);
    [playButton setTarget:self selector:@selector(play:)];
    [self addChild:playButton];

	return self;
}

#pragma mark - Buttons

- (void)play:(id)sender
{
    [[CCDirector sharedDirector] replaceScene:[MAGGameScene scene]
                               withTransition:[CCTransition transitionPushWithDirection:CCTransitionDirectionInvalid duration:1.0f]];
}

@end