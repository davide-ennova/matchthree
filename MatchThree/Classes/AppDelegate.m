//
//  AppDelegate.m
//  MatchThree
//
//  Created by Vincenzo Alampi on 19/11/14.
//  Copyright None 2014. All rights reserved.
//
// -----------------------------------------------------------------------

#import "AppDelegate.h"
#import "MAGIntroScene.h"

@implementation AppDelegate

-(BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions
{

    [self setupCocos2dWithOptions:@{
                                    CCSetupShowDebugStats: @(NO),
                                    CCSetupScreenOrientation: CCScreenOrientationPortrait,
                                    //		CCSetupPixelFormat: kEAGLColorFormatRGB565,
                                    //		CCSetupScreenMode: CCScreenOrientationPortrait,
                                    //		CCSetupAnimationInterval: @(1.0/30.0),
                                    //		CCSetupFixedUpdateInterval: @(1.0/180.0),
                                    //		CCSetupTabletScale2X: @(YES),
	}];
	
	return YES;
}

-(CCScene *)startScene
{
	return [MAGIntroScene scene];
}


@end