//
//  MAGGameScene.h
//  MatchThree
//
//  Created by Vincenzo Alampi on 19/11/14.
//  Copyright None 2014. All rights reserved.
//
// -----------------------------------------------------------------------

#import "cocos2d.h"
#import "cocos2d-ui.h"

@interface MAGGameScene : CCScene

+ (MAGGameScene *)scene;

@end