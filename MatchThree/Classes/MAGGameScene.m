//
//  MAGGameScene.m
//  MatchThree
//
//  Created by Vincenzo Alampi on 19/11/14.
//  Copyright None 2014. All rights reserved.
//
// -----------------------------------------------------------------------

#import "MAGGameScene.h"
#import "MAGGameController.h"

@interface MAGGameScene()

@property (strong, nonatomic) MAGGameController *gameController;

@end

@implementation MAGGameScene

+ (MAGGameScene *)scene
{
    return [[self alloc] init];
}

- (id)init
{
    self = [super init];
    if (!self) return(nil);
    
    CCSprite *bg = [CCSprite spriteWithImageNamed:@"bg.png"];
    bg.positionType = CCPositionTypeNormalized;
    bg.position = ccp(0.5f, 0.5f);
    
    [self addChild:bg];
    
    self.gameController = [MAGGameController gameWithLevel:1 scene:self];
    
	return self;
}

@end