//
//  MAGDrawNode.h
//  MatchThree
//
//  Created by Vincenzo Alampi on 23/11/14.
//  Copyright (c) 2014 None. All rights reserved.
//

#import "CCDrawNode.h"

@class MAGSelection;

@interface MAGSelectionDrawNode : CCDrawNode

+ (MAGSelectionDrawNode*) drawSelection:(MAGSelection*)selection;

@end