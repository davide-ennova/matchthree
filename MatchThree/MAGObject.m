//
//  MGObject.m
//  MatchThree
//
//  Created by Vincenzo Alampi on 21/11/14.
//  Copyright (c) 2014 None. All rights reserved.
//

#import "MAGObject.h"

@interface MAGObject()

@end

@implementation MAGObject

+ (MAGObject*) createObject:(MAGObjectType)type
{
    return [[self alloc] initWithObjectType:type];
}

- (id) initWithObjectType:(MAGObjectType)type
{
    if (self = [super init])
    {
        self.type = type;
        
        NSString *spriteName = [self convertTypeToSprite:self.type];
        
        self.sprite = [CCSprite spriteWithImageNamed:spriteName];
        self.sprite.anchorPoint = ccp(0.5f, 0.5f);
    }
    
    return self;
}

- (NSString*) convertTypeToSprite:(MAGObjectType)type
{
    NSString *typeName = @"amber";
    
    switch (type)
    {
        case amber: typeName = @"amber"; break;
        case emerald: typeName = @"emerald"; break;
        case prism: typeName = @"prism"; break;
        case sapphire: typeName = @"sapphire"; break;
        case ruby: typeName = @"ruby"; break;

        default: break;
    }
    
    return [NSString stringWithFormat:@"%@.png", typeName];
}

@end