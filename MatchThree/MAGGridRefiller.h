//
//  MAGGridRefiller.h
//  MatchThree
//
//  Created by Vincenzo Alampi on 26/11/14.
//  Copyright (c) 2014 None. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "MAGGrid.h"

@class MAGObject;

@protocol MAGGridRefillProtocol <NSObject>

- (void) refillCol:(NSInteger)col row:(NSInteger)row;
- (void) changeStateTo:(GridState)state;

@end

@interface MAGGridRefiller : NSObject

@property (assign, nonatomic) NSInteger stateCounter;
@property (assign, nonatomic) id <MAGGridRefillProtocol> delegate;

+ (MAGGridRefiller*) createRefiller;

- (void) setupColumnsToRefill:(NSMutableArray*)columns data:(NSMutableArray*)dataContent;

- (void) startPullDownPhase;
- (void) startRefillEmptyObjectsPhase;
- (void) finishRefillPhase;

@end