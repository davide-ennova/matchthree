//
//  MAGObjectsAnimator.m
//  MatchThree
//
//  Created by Vincenzo Alampi on 22/11/14.
//  Copyright (c) 2014 None. All rights reserved.
//

#import "MAGObjectsAnimator.h"
#import "MAGGridRefiller.h"

#define K_FALLING_SPEED 25.0f;

@implementation MAGObjectsAnimator

+ (void) selectAnimation:(CCNode*)node
{
    id action = [CCActionScaleTo actionWithDuration:0.075f scale:1.05f];
    id action2 = [CCActionScaleTo actionWithDuration:0.075f scale:0.8f];
    
    [node runAction:[CCActionSequence actions:action, action2, nil]];
}

+ (void) deselectAnimation:(CCNode*)node
{
    id action = [CCActionScaleTo actionWithDuration:0.1f scale:1.0f];
    
    [node runAction:action];
}

+ (void) collectAnimation:(CCNode*)node notify:(MAGGridRefiller*)refiller
{
    id action = [CCActionScaleTo actionWithDuration:0.075f scale:1.0f];
    id action2 = [CCActionScaleTo actionWithDuration:0.1f scale:0.0f];
    
    refiller.stateCounter ++;

    id action3 = [CCActionCallBlock actionWithBlock:^() {
        
        [node removeFromParentAndCleanup:YES];
        refiller.stateCounter --;
        
        if (refiller.stateCounter == 0)
        {
            [refiller startPullDownPhase];
        }
    }];
    
    [node runAction:[CCActionSequence actions:action, action2, action3, nil]];
}

+ (void) fallingAnimation:(CCNode*)node toPosition:(CGPoint)position notify:(MAGGridRefiller*)refiller
{
    float distance = ccpDistance(node.position, position) * 0.01f;
    float duration = distance / K_FALLING_SPEED;
    
    id action = [CCActionMoveTo actionWithDuration:duration position:position];
    
    refiller.stateCounter ++;
    
    id action2 = [CCActionCallBlock actionWithBlock:^() {
        
        refiller.stateCounter --;
        
        if (refiller.stateCounter == 0)
        {
            [refiller startRefillEmptyObjectsPhase];
        }
    }];

    [node runAction:[CCActionSequence actions:action, action2, nil]];
}

+ (void) refillingAnimation:(CCNode*)node notify:(MAGGridRefiller *)refiller
{
    CGPoint finalPosition = node.position;
    CGPoint startingPosition = ccp(node.position.x, node.position.y + node.boundingBox.size.height * 2.0f);
   
    node.position = startingPosition;
    
    float distance = ccpDistance(startingPosition, finalPosition) * 0.01f;
    float duration = distance / K_FALLING_SPEED;
    
    id action = [CCActionFadeIn actionWithDuration:0.25f];
    
    [node runAction:action];

    id action2 = [CCActionMoveTo actionWithDuration:duration position:finalPosition];
    
    refiller.stateCounter ++;
    
    id action3 = [CCActionCallBlock actionWithBlock:^() {
        
        refiller.stateCounter --;
        
        if (refiller.stateCounter == 0)
        {
            [refiller finishRefillPhase];
        }
    }];

    [node runAction:[CCActionSequence actions:action2, action3, nil]];
}

@end